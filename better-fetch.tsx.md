```js
type CallbackType = (response: any, error?: string | null) => void;
type SuccessCallbackType = (response: any) => void;
type ErrorCallbackType = (error: string) => void;

const retrieve = (
  url: string,
  request: RequestInterface,
  callback: CallbackType
) => {
  fetch(url, request)
    .then((raw) => raw.json())
    .then((response) => callback(response)) // Verbose closure for readability
    .catch((error) => callback(null, error));
};

const request = (
  method: MethodType,
  headers = RequestHeaders,
  body: any = null
): RequestInterface => {
  const retrieveDataObject: RequestInterface = {
    method,
    headers: {
      "Content-Type": "application/json",
      ...headers,
    },
  };

  body && (retrieveDataObject["body"] = JSON.stringify(body));

  return retrieveDataObject;
};

export const compactFetch = (
  method: MethodType,
  headers?: RequestHeaders,
  body?: any
) => {
  return (url: string, callback: CallbackType) => {
    retrieve(url, request(method, headers, body), callback);
  };
};

compactFetch("GET")("http://example.com/movies.json", (data, error) => {
  if (!error && data !== null) {
    // success scenario
  }
});

/* or */

const get = compactFetch("GET");

get("http://example.com/movies.json", (data, error) => {
  if (!error && data !== null) {
    // success scenario
  }
});

const DATA = { name: "example", rating: 10 };

compactFetch(
  "POST",
  {},
  DATA
)("http://example.com/newMovie", (data, error) => {
  if (!error && data !== null) {
    // success scenario
  }
});

/* or */

const post = compactFetch("POST", {}, DATA);

post("http://example.com/newMovie", (data, error) => {
  if (!error && data !== null) {
    // success scenario
  }
});
```
