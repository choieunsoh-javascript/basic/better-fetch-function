import fetch from "node-fetch";

(async () => {
  try {
    const response = await fetch("https://jsonplaceholder.typicode.com/users", {
      method: "GET",
    });
    const data = await response.json();
    console.log(data);
  } catch (error) {
    console.error(error);
  }
})();
